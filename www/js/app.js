var socket = io();

var JobsList = React.createClass({

  getInitialState: function() {
    return {
      jobs: []
    };
  },

  componentDidMount: function() {
    socket.emit('/jobs');
    socket.on('/jobs', function(data){

      if (this.isMounted()) {
        this.setState({
          jobs: data
        });
      }
    }.bind(this));
  },

  render: function() {
    return (
      <ul>
        {this.state.jobs.map(function(job) {
          return <li>{job.name}</li>;
        })}
      </ul>
    );
  }
});

var BuildsList = React.createClass({

  getInitialState: function() {
    return {
      builds: []
    };
  },

  componentDidMount: function() {
    socket.emit('/builds');
    socket.on('/builds', function(data){
      if (this.isMounted()) {
        console.log(data);
        this.setState({
          builds: data
        });
      }
    }.bind(this));
  },

  render: function() {
    console.log(this);
    return (
      <ul>
        {this.state.builds.map(function(build) {
          return <li>{build.$.lastBuildStatus} - {build.$.name} ({build.$.activity})</li>;
        })}
      </ul>
    );
  }
});

React.render(
  <JobsList />,
  document.getElementsByClassName('jobs')[0]
);

React.render(
  <BuildsList />,
  document.getElementsByClassName('builds')[0]
);
