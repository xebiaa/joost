'use strict';

let jenkinsapi  = require('jenkins-api')
,   jenkins     = jenkinsapi.init('http://localhost:8080/')
,   express     = require('express')
,   app         = express()
,   path        = require('path')
,   request     = require('request')
,   xmlParse    = require('xml2js').parseString
,   http        = require('http').Server(app)
,   io          = require('socket.io')(http)
,   host        = 'http://localhost:8080/'
;

// Serve web directory
app.use(express.static(path.join(__dirname, './www')));

jenkins.all_jobs(function(err, data) {
  if (err){ return console.log(err); }
  //console.log(data)
});

jenkins.job_info('My Job', function(err, data) {
  if (err){ return console.log(err); }
  //console.log(data)
});

io.on('connection', function (socket) {

    socket.on('/jobs', function () {

      jenkins.all_jobs(function(err, data) {
        if (err){ return console.log(err); }
        io.emit('/jobs', data);
      });

    });

    socket.on('/builds', function () {
      // There is no API path for builds.. but there is a xml
      let buildsPath = host + 'view/All/cc.xml';

      request.get(buildsPath, function (error, response, body) {
        if (!error && response.statusCode == 200) {
          xmlParse(body, function (err, data) {
              console.log(data);
              if (err){ return console.log(err); }
              io.emit('/builds', data.Projects.Project);
            });
          }
      });

    });

});

http.listen(3000, function () {
    console.log('listening on: 3000');
});
